﻿using Serilog;
using Serilog.Core;

string? sentryDSN = Environment.GetEnvironmentVariable("SENTRY_DSN");

if (sentryDSN is null)
{
    throw new Exception("SENTRY_DSN must be set to the DSN of the GitLab error tracking back end as environment variable before running");
}

Console.WriteLine($"Initializing...");

LoggerConfiguration configuration = new();

configuration.WriteTo.Sentry(options =>
{
    options.Dsn = sentryDSN;
    options.SampleRate = 1.0f;
    options.AttachStacktrace = true;
    options.SendDefaultPii = false;

    options.Debug = true;
    options.DiagnosticLogger = new SentrySDKDiagnosticsConsoleLogger();
    options.Release = $"dotnet-serilog-gitlab-sentry-bug-reproducible-example@1.0.0";
});

Logger logger = configuration.CreateLogger();

Console.WriteLine("Logging error to Serilog...");

logger.Error(new Exception("Test exception using logger directly"), "Test exception using logger directly");

Console.WriteLine("Done, exiting.");