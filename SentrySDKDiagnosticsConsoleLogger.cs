﻿using Sentry.Extensibility;

public sealed class SentrySDKDiagnosticsConsoleLogger : IDiagnosticLogger
{
    public bool IsEnabled(SentryLevel level)
    {
        return true;
    }

    public void Log(SentryLevel logLevel, string message, Exception? exception = null, params object?[] args)
    {
        if (logLevel is SentryLevel.Debug)
        {
            Console.WriteLine(string.Format($"SENTRY SDK: DEBUG: {message} {exception}", args));
        }
        else if (logLevel is SentryLevel.Info)
        {
            Console.WriteLine(string.Format($"SENTRY SDK: INFO: {message} {exception}", args));
        }
        else if (logLevel is SentryLevel.Warning)
        {
            Console.WriteLine(string.Format($"SENTRY SDK: WARNING: {message} {exception}", args));
        }
        else if (logLevel is SentryLevel.Error)
        {
            Console.WriteLine(string.Format($"SENTRY SDK: ERROR: {message} {exception}", args));
        }
        else if (logLevel is SentryLevel.Fatal)
        {
            Console.WriteLine(string.Format($"SENTRY SDK: FATAL: {message} {exception}", args));
        }
    }
}
