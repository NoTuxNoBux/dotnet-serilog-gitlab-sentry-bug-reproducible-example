Reproducible example of Serilog logging an exception through the Sentry back end that includes a stacktrace. The stacktrace isn't included in the GitLab error tracking overview when the internal Sentry back end is used.

See also https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2855 for more details.

# Running

Requires .NET 8 to run.

```bash
export SENTRY_DSN="https://<API_TOKEN>@observe.gitlab.com:443/errortracking/api/v1/projects/<PROJECT_ID>"
dotnet run
```